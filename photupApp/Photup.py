from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from glob import glob
from datetime import datetime

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import threading
import time
import ast
import tkinter.ttk as ttk
import os
import json
import uuid
import playsound
import hashlib
import math
import sys

# TODO:
# Een installer die Photup for Windows en zonodig Python installeert.

class MainWindow(Tk):
    def __init__(self) -> None:
        "Function for initilising all components."
        super().__init__()
        
        # Creating variables.
        self.upload_queue = []

        # Call functions.
        self.setWindowSettings()
        self.createWidgets()
        

    def resource_path(self, relative_path):
        "Get absolute path to resource, works for dev and for PyInstaller"
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)


    def setWindowSettings(self):
        "Funtion for setting the window settings"
        w = self.winfo_screenwidth()
        h = self.winfo_screenheight()
        self.geometry(f"600x300+{int(w/2)-300}+{int(h/2)-150}")
        self.minsize(600, 300)
        self.iconbitmap(self.resource_path("icon.ico"))
        self.title("Photup")


    def getDir(self) -> None:
        "Function for getting image paths."
        # Getting the directory path from the user.
        dir = filedialog.askdirectory()

        # Check if directory was selected.
        if dir == "":
            return

        # Put the directory path in the entry.
        self.path_var.set(dir)


    def addUpload(self) -> None:
        "Function for adding uploads to the queue"
        # Check if selected external sotrage inserted.
        if os.path.exists(self.path_var.get().split("/")[0]+"/") == False:
            messagebox.showinfo(title="Expected external storage not found", message="Please insert Expected external storage.")
            return

        # Getting upload size.
        images = glob(f"{self.path_var.get()}/*.jpg")
        sizes = [os.path.getsize(f) for f in images]

        # Create upload data.
        data = {"name": str(uuid.uuid4().hex), "drive":self.drive_var.get(), "path":self.path_var.get(), "amount": len(images), "size": self.getStorageString(sum(sizes)), "done": 0,"status": "Queued"}

        # Getting canceled uploads amount.
        canceled = 0
        for upload in self.upload_queue:
            if upload["status"] == "Canceled":
                canceled += 1

        # Add data to list and listbox.
        self.listbox.insert(len(self.upload_queue) - canceled, f"name: {data['name']}, amount: {data['amount']}, size: {data['size']}, status: {data['status']}")
        self.upload_queue.insert(len(self.upload_queue) - canceled, data)

    
    def removeUploads(self) -> None:
        "Function for removing the selected upload."
        # Check if window is already open.
        if "upload_window" in dir(self):
            return

        # Get the selected indexes.
        indexes = list(self.listbox.curselection())

        # Check if indexes are selected.
        if indexes == []:
            return

        # Sort the indexes in reverse.
        indexes.sort(reverse=True) 

        # Change upload upload status to canceled.
        for index in indexes:
            if self.upload_queue[index]["status"] == "Done" or self.upload_queue[index]["status"] == "Canceled":
                continue
            self.listbox.delete(index)
            self.listbox.insert("end", f"name: {self.upload_queue[index]['name']}, amount: {self.upload_queue[index]['amount']}, size: {self.upload_queue[index]['size']}, status: Canceled")
            self.listbox.itemconfig("end", bg="red")
            self.upload_queue[index]["status"] = "Canceled"
            self.upload_queue.insert(len(self.upload_queue), self.upload_queue[index])
            self.upload_queue.remove(self.upload_queue[index])


    def uploadFiles(self)-> None:
        "Function for uploading the files to google drive."
        # Check if window is already open.
        if "upload_window" in dir(self):
            return

        # Check if there are uploads to do.
        if len(self.upload_queue) == 0:
            return
        stop = True
        for upload in self.upload_queue:
            if upload["status"] != "Canceled" and upload["status"] != "Done":
                stop = False
        if stop:
            return

        # Call the upload window.
        self.upload_window = UploadingWindow(self)
        self.upload_window.protocol("WM_DELETE_WINDOW", self.upload_window.closing)


    def closing(self) -> None:
        "Function for closing the application."
        # Save directory path and drive code as default + queue list.
        f = open("config.json", "w")
        data = {"drive_code": self.drive_var.get(), "image_path": self.path_var.get(), "upload_queue": self.upload_queue}
        json.dump(data, f)

        # Closing the application.
        self.quit()


    def getStorageString(self, bytes):
        "Function that convets bytes to other units."
        # Check if bytes are 0.
        if bytes == 0:
            return "0 B"

        # convert to 
        size_name = ("B", "KB", "MB", "GB")
        i = int(math.floor(math.log(bytes, 1024)))
        p = math.pow(1024, i)
        s = round(bytes / p, 2)
        return f"{s} {size_name[i]}"


    def createWidgets(self) -> None:
        "Function for creating the widgets."
        # Read config.txt.
        f = open("config.json")
        data = json.load(f)

        # Check if data has status uploading.
        for upload in data["upload_queue"]:
            if upload["status"] == "Uploading":
                upload["status"] = "Stoped"

        # Remove uploads with status done or canceled.
        _list = []
        delete = []
        for index, upload in enumerate(data["upload_queue"]):
            if upload["status"] == "Done" or upload["status"] == "Canceled":
                delete.append(index)
                continue
            _list.append(f"name: {upload['name']}, amount: {upload['amount']}, size: {upload['size']}, status: {upload['status']}")
        for i in reversed(delete):
            del data["upload_queue"][i]

        # Update the update queue.
        self.upload_queue = data["upload_queue"]

        # Creating the widgets.
        self.drive_label = Label(self, text="Foler id:")
        self.drive_var = StringVar(self, value=data["drive_code"])
        self.drive_entry = Entry(self, textvariable=self.drive_var)
        self.path_label = Label(self, text="Path to folder:")
        self.path_var = StringVar(self, value=data["image_path"])
        self.path_entry = Entry(self, textvariable=self.path_var)
        self.browse_button = Button(self, text="Browse folder", command=lambda: self.getDir())
        self.listbox_label = Label(self, text="Queued up uploads")
        self.listbox_var = StringVar(self, value=_list)
        self.listbox = Listbox(self, listvariable=self.listbox_var, selectmode=EXTENDED)
        self.scrollbar = Scrollbar(self)
        self.add_upload_button = Button(self, text="Add\nupload", command=lambda: self.addUpload())
        self.remove_upload_button = Button(self, text="Cancel\nuploads", command=lambda: self.removeUploads())
        self.upload_button = Button(self, text="Upload files", height=2, command=lambda: self.uploadFiles())

        # Linking scrollbar to listbox.
        self.listbox.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.listbox.yview)

        # Placing the widgets.
        self.drive_label.grid(row=0, column=0, sticky="nesw")
        self.drive_entry.grid(row=0, column=1, columnspan=3, sticky="nesw")
        self.path_label.grid(row=1, column=0, sticky="nesw")
        self.path_entry.grid(row=1, column=1, columnspan=2, sticky="nesw")
        self.browse_button.grid(row=1, column=3, sticky="nesw")
        self.listbox_label.grid(row=2, column=0, columnspan=3, sticky="nesw")
        self.listbox.grid(row=3, rowspan=2, column=0, columnspan=2, sticky="nesw")
        self.scrollbar.grid(row=3, rowspan=2, column=2, sticky="nesw")
        self.add_upload_button.grid(row=3, column=3, sticky="nesw")
        self.remove_upload_button.grid(row=4, column=3, sticky="nesw")
        self.upload_button.grid(row=5, column=0, columnspan=4, sticky="nesw")

        # Make the widgets responsive.
        self.columnconfigure(1, weight=1)
        self.rowconfigure(3, weight=1)
        self.rowconfigure(4, weight=1)


class UploadingWindow(Toplevel):
    def __init__(self, parent) -> None:
        "Function for initilising all components."
        super().__init__()

        # Creating variables.
        self.parent = parent
        self.drive_id = parent.drive_var.get()
        self.stop_thread = False

        # Call functions.
        self.createWidgets()
        self.setWindowSettings()

        # Start the tread.
        self.tread = threading.Thread(target=self.uploadFiles, daemon=True)
        self.tread.start()


    def setWindowSettings(self):
        "Funtion for setting the window settings"
        x = self.parent.winfo_x()+int(self.parent.winfo_width()/2)-120
        y = self.parent.winfo_y()+int(self.parent.winfo_height()/2)
        self.geometry(f"+{x}+{y}")
        self.iconbitmap(self.parent.resource_path("icon.ico"))
        self.title("Uploading")
        self.resizable(False, False) 


    def createWidgets(self) -> None:
        "Function for creating the widgets."
        # Creating the widgets.
        self.queue_label = Label(self, text="Upload: 0/0")
        self.status_label = Label(self, text="Uploaded images: 0/0, estimated time: 0.0")
        self.progressbar = ttk.Progressbar(self, mode='determinate')

        # Placing the widgets.
        self.queue_label.grid(row=0, column=0, sticky="nesw")
        self.status_label.grid(row=1, column=0, sticky="nesw")
        self.progressbar.grid(row=2, column=0, sticky="nesw")


    def uploadFiles(self) -> None:
        "Function for uploading the files"
        # Get the amount of canceled and done uploads.
        uploaded = [1,0]
        min = [0,0]
        for upload in self.parent.upload_queue:
            if upload["status"] == "Canceled":
                min[0] += 1
            elif upload["status"] == "Done":
                min[1] += 1

        # Make drive stuff.
        gauth = GoogleAuth()           
        drive = GoogleDrive(gauth) 

        # Update label.
        self.queue_label.config(text=f"Upload: {uploaded[0]-min[1]}/{len(self.parent.upload_queue)-sum(min)}")
        for index in range(0, len(self.parent.upload_queue)):
            self.current_index = index

            # Check if upload is already done or canceled.
            if self.parent.upload_queue[index]["status"] == "Done":
                uploaded[0] += 1
                self.queue_label.config(text=f"Upload: {uploaded[0]-min[1]}/{len(self.parent.upload_queue)-sum(min)}")
                continue
            elif self.parent.upload_queue[index]["status"] == "Canceled":
                break

            # Check if upload was stoped.
            foldername = self.parent.upload_queue[index]["name"]
            resume_id = ""
            if self.parent.upload_queue[index]["status"] == "Stoped":
                file_list = drive.ListFile({'q': f"'{self.drive_id}' in parents and trashed=false"}).GetList()
                for file in file_list:
                    if foldername in file["title"]:
                        resume_id = file["id"]
                        folder = file

            # Update label.
            self.queue_label.config(text=f"Upload: {uploaded[0]-min[1]}/{len(self.parent.upload_queue)-sum(min)}")

            # Set status to uploading.
            self.parent.upload_queue[index]["status"] = "Uploading"
            _list = list(ast.literal_eval(self.parent.listbox_var.get()))
            _list[index] = f"{_list[index].split(',')[0]},{_list[index].split(',')[1]},{_list[index].split(',')[2]}, status: Uploading"
            self.parent.listbox_var.set(_list)

            # check for external storage.
            if os.path.exists(self.parent.upload_queue[index]["path"]) == False:
                messagebox.showinfo(title="Expected external storage not found", message="Please insert Expected external storage.")
                self.closing()
                return

            # Get image paths.
            image_paths = glob(f"{self.parent.upload_queue[index]['path']}/*.jpg")

            # Update progressbar and label.
            self.progressbar.config(value=0, maximum=len(image_paths))
            self.status_label.config(text=f"Uploaded images: {uploaded[1]}/{len(image_paths)}, estimated time: 0.0")

            # Create new folder.
            if resume_id == "":
                folder = drive.CreateFile({'parents': [{'id': self.drive_id}], 'title': foldername, 'mimeType': 'application/vnd.google-apps.folder'})
                folder.Upload()

            # Change the image names.
            image_names = []
            for i, image in enumerate(image_paths):
                name = f"{foldername}_img{i+1:06}.JPG"
                image_names.append(name)

            # Create extra files.
            file_list = drive.ListFile({'q': f"'{folder['id']}' in parents and trashed=false"}).GetList()
            file_names = [file["title"] for file in file_list]

            if f"{foldername}_init.txt" not in file_names:
                file = drive.CreateFile({'parents': [{'id': folder['id']}], 'title': f'{foldername}_init.txt'})
                file.SetContentString(",".join(image_names))
                file.Upload()

            if f"{foldername}_md5.txt" not in file_names:
                # Get hashed strings.
                lines = []
                for id, image in enumerate(image_paths):
                    f = open(image, 'rb')
                    data = f.read()
                    md5 = hashlib.md5(data).hexdigest()
                    lines.append(f"{md5}  {image_names[id]}")
                # Creating the text for the file
                sep = "\n"
                text = sep.join(lines)

                file = drive.CreateFile({'parents': [{'id': folder['id']}], 'title': f'{foldername}_md5.txt'})
                file.SetContentString(text)
                file.Upload()

            times = []
            failed = 0
            for i, image in enumerate(image_paths):
                # check if images are already uploaded and update progressbar and label.
                if image_names[i] in file_names:
                    self.progressbar["value"] += 1
                    uploaded[1] += 1
                    self.status_label.config(text=f"Uploaded images: {uploaded[1]}/{len(image_paths)}, estimated time: 0.0")
                    continue
                
                # Upload images.
                t0 = time.time()
                try:
                    gfile = drive.CreateFile({'parents': [{'id': folder['id']}], 'title': image_names[i]})
                    
                    # Read file and set it as the content of this instance.
                    gfile.SetContentFile(image)

                    # Upload the file.
                    gfile.Upload()

                    # Save directory path and drive code as default + queue list.
                    f = open("config.json", "w")
                    data = {"drive_code": self.parent.drive_var.get(), "image_path": self.parent.path_var.get(), "upload_queue": self.parent.upload_queue}
                    json.dump(data, f)
                except:
                    failed += 1
                t1 = time.time()
                times.append(t1-t0)

                # Check if thread needs to be stopped.
                if self.stop_thread == True:
                    return

                # Update progressbar and labels.
                self.progressbar["value"] += 1
                uploaded[1] += 1
                self.parent.upload_queue[index]["done"] = uploaded[1]
                self.status_label.config(text=f"Uploaded images: {uploaded[1]}/{len(image_paths)}, estimated time: {self.getEstimatedTime(times, len(times), len(image_paths))}")
            uploaded = [uploaded[0]+1 ,0]

            # Create text for file.
            sizes = [os.path.getsize(f) for f in image_paths]
            text = (f"Successful uploads: {len(image_paths)-failed} of {len(image_paths)}.\n" + 
                    f"Time: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n" +
                    f"Finished in {self.getTimeString(int(sum(times)), 0)} at an average of {self.getTimeString(int(sum(times) / len(times)), 0)} per image.\n" +
                    f"Total uploaded file size equals {self.parent.getStorageString(sum(sizes))} at an average of {self.parent.getStorageString(int(sum(sizes) / len(sizes)))} per image.\n" +
                    f"Kazje_id: 3")

            # Create extra file.
            if f"{foldername}_exit.txt" not in file_names:
                file = drive.CreateFile({'parents': [{'id': folder['id']}], 'title': f'{foldername}_exit.txt'})
                file.SetContentString(text)
                file.Upload()

            # Set status to done.
            self.parent.upload_queue[index]["status"] = "Done"
            _list = list(ast.literal_eval(self.parent.listbox_var.get()))
            _list[index] = f"{_list[index].split(',')[0]},{_list[index].split(',')[1]},{_list[index].split(',')[2]}, status: Done"
            self.parent.listbox.itemconfig(index, bg="green")
            self.parent.listbox_var.set(_list)
            self.parent.upload_queue[index]["status"] = "Done"

        # Remove window variable for main class.
        del self.parent.upload_window

        # Destroy window.
        self.destroy()
        playsound.playsound(self.parent.resource_path("buzzer.wav"))


    def getEstimatedTime(self, times, done, total) -> str:
        "Function that gets the estimated time it will take to finish the uploads."
        time_till_now = sum(times)
        avarage_time_per_image = time_till_now / done
        time_to_do = avarage_time_per_image * (total - done)
        time_string = self.getTimeString(int(time_to_do), 1)
        return time_string


    def getTimeString(self, seconds, mode) -> str:
        "Function that converts seconds to hours, minutes and seconds."
        if seconds < 59:
            return f"{seconds} seconds"
        elif seconds >= 60 and seconds < 3599:
            minutes = int(seconds/60)
            seconds = seconds%60
            if mode == 0:
                return f"{minutes} minutes and {seconds} seconds"
            if mode == 1:
                return f"{minutes} minutes"
        elif seconds >= 3600:
            hours = int(seconds/3600)
            minutes = int((seconds%3600)/60)
            seconds = (seconds%3600)%60
            if mode == 0:
                return f"{hours} hours, {minutes} minutes and {seconds} seconds"
            if mode == 1:
                return f"{hours} hours and {minutes} minutes"


    def closing(self) -> None:
        "Function for closing the upload window."
        self.stop_thread = True

        # Change status of current upload to stoped.
        self.parent.upload_queue[self.current_index]["status"] = "Stoped"
        _list = list(ast.literal_eval(self.parent.listbox_var.get()))
        _list[self.current_index] = f"{_list[self.current_index].split(',')[0]},{_list[self.current_index].split(',')[1]},{_list[self.current_index].split(',')[2]}, status: Stoped"
        self.parent.listbox_var.set(_list)

        # Remove window variable for main class.
        del self.parent.upload_window

        # Destroy window.
        self.destroy()
            


if __name__ == "__main__":
    window = MainWindow()
    window.protocol("WM_DELETE_WINDOW", window.closing)
    window.mainloop()