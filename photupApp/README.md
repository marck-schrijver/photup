# Gebruikers handleiding  
## Setup  
Voordat je de app kunt gaan gebruiken moet je het **client_secrets.json** bestand in de root folder van de app plaatsen.  
Om dit te doen ga je eerst naar https://console.cloud.google.com en log je in.  
Daarna maak je een nieuw project aan.  
Vervolgens ga je naar **Enabled APIs & services** en druk je op de **+ Enable apis** and services knop.  
Hier zoek je naar google drive api en druk je op Enable.  
Ga nu naar **OAuth consent screen** en volg de stappen tot test users.(zorg er wel voor dat je de app op in productie zet, anders vervalt de token elke 7 dagen)  
Hier voeg je je email toe en volg je de stappen verder tot **add users**.  
Hier vul je je e-mail die aan de drive gekoppelt is in en daarna volg je de stappen verder.  
Ga nu naar **Credentials** en druk op de **+ Create Credentials** knop en selecteer OAuth client ID.  
Hier selecteer je application type als desktop app.  
Nu popt er een schrem op waar je een json file kan downloaden.  
dit is je client secret file.  
hernoem het naar **client_secrets.json** en plaats het in de root folder van de app.  
En tot slot ga je naar **settings.yaml** en pas je de client_id en client_secret aan.  
De eerste keer als je iets upload moet je nog inlogen maar daarna niet meer.  

## Gebruik  
Wanneer je de app opent plaats de drive folder id in de drive code textbox.  
Je kunt de folder id vinden in de URL als je in de folder zit. (bv. https://drive.google.com/drive/folders/**dit stuk**)  
Je kunt met de "**Browse folder**" knop een folder selecteren met de foto's die je wilt uploaden.  
Daarna kun je met de "**Add upload**" knop een upload toevoegen aan de list.  
Je kunt met de "**Remove upload**" de geselecteerde upload ook weer een upload uit de lijst halen.  
Tot slot kun je met de "**Upload files**" knop de images uploaden naar de aangegeven folder.  
Om het uploaden te stoppen kun je op het kruisje drukken om het upload window te sluiten.  
Je kunt een upload hervatten door nog een keer op de "**Upload files**" knop te drukken.  

## Waarschuwing
Als de app niet upload moet je naar de root gaan en de file **Credentials.json** verwijderen en wanneer je weer upload inloggen op je account.  
Dit maakt een nieuwe **Credentials.json** aan.
